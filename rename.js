const fs = require('fs');
const path = require('path');

const argv = process.argv.slice(2);
const folderName = argv[0];
const startToRemove = +argv[1];
const toFolder = argv[2];

function renameFile(folderName, file, newFile, toFolder) {
    return fs.rename(path.join(
        folderName, file),
        path.join(toFolder 
            ? `${folderName}/${toFolder}` 
            : folderName, newFile),
        (err) => {
        if (err) throw err;
        console.log(`Renamed - ${file}`);
    });
}

fs.readdir(path.join(__dirname, folderName), (err, files) => {
    if (err) throw err;
    console.log(files)
    files.map(file => {
        if (path.extname(file)) {
            const newFile = file.replace(/(\s)/g, "").split("").splice(startToRemove).join("");
            if (toFolder) {
                fs.mkdir(path.join(__dirname, folderName, toFolder), { recursive: true }, (err) => {
                    if (err) throw err;
                    renameFile(folderName, file, newFile, toFolder);
                });
            } else {
                renameFile(folderName, file, newFile);
            }
        }
    });
});